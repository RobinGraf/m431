# Verknüpfungen


## Dokumentation
-> [Dokumenatition](https://gitlab.com/RobinGraf/m431/-/blob/main/Dokumentation/Dokumentation.pdf)

## Lernjournal 
-> [Lernjournal Robin](https://docs.google.com/document/d/145JpbIWo5mwe7dIjrL0X5QRfEAfDyMmS1Aqo-ZrFt4c/edit)

-> [Lernjournal Linus](https://docs.google.com/document/d/1hfUm4mYqRQWJarfg4kKaU7tcmWcjA25jIewWgw8IvUc/edit)

## Projektantrag
-> [Projektantrag](https://gitlab.com/RobinGraf/m431/-/blob/main/Projektantrag/Projektantrag.pdf)

## Nutzwertanalyse
-> [Nutzwertanalyse](https://gitlab.com/RobinGraf/m431/-/blob/main/Nutzwertanalyse/Nutzwertanalyse.png)

## Smart-Ziele
-> [Smart-Ziele](https://gitlab.com/RobinGraf/m431/-/blob/main/Smart-Ziele/Smart-Ziele.png)

## Website 
-> [Website](https://linusdeppeler.wixsite.com/website)

## README
-> [README](https://gitlab.com/RobinGraf/m431/-/blob/main/README.md)


#
 von Linus Deppeler und Robin Graf 
